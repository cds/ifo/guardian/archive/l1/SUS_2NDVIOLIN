from guardian import GuardState
from guardian import NodeManager
from time import sleep
import subprocess
import time
import os
import cdsutils

# v1 - S. Aston - 04/01/2019
# Script for LLO to manage SUS violin modes

# v2 - S. Aston - 04/03/2019
# Added DAMP ALL and UNDAMP ALL states
# Added some notifications for completing DAMP and UNDAMP all completion and if the overall VM damping switch is off

# v3 - S. Aston - 04/04/2019
# Enabled "goto" states for each specific violin mode order damping or un-damping, but leave damp/undamp all as requestable states only

# v4 - S. Aston - 04/09/2019
# Make almost everything a "goto" state, make "damp_3rd" nominal and remove "damp_all"
# Reduced notify sleeps from 5 to 3 seconds to help expedite things

# v5 - S. Aston - 04/11/2019
# Based on SUS_VIOLIN.py generated a dedicated Guardian node for just 2nd order violin mode damping

# v6 - S. Aston - 05/21/2019
# Added violin mode warnings to monitor damping of modes above a defined threshold

# v7 - S. Aston - 06/25/2019
# Enabled violin mode monitoring to automatically shut down damping of modes above a defined threshold to try and avoid a lock-loss

# TODO:
# Consider adding some alarms and alerts after reaching a threshold?
# Can we speed up setting up filters by restoring EDB snapshots of violin modes?

# Define all QUAD suspensions
quads = ['ITMX','ITMY','ETMX','ETMY']

# Define 1st 2nd and 3rd order mode numbers
modes1 = ['3', '4', '5', '6']
modes2 = ['11', '12', '13', '14']
modes3 = ['21', '22', '23', '24']

# Nominal operational state
nominal = 'DAMP_2ND'

# Initial request on initialization
request = 'IDLE'

class INIT(GuardState):
    def main(self):
        return True

class IDLE(GuardState):
    goto = True
    request = True
    
    def main(self):
        return True

class DAMP_2ND(GuardState):
    goto = True
    request = True

    def main(self):
        #if ezca['SUS-ITMX_L2_DAMP_OUT_SW'] == 0:
        #    notify("Violin mode damping switch is disabled")

        for sus in quads:
            for mode in modes2:
		# Zero the gains for all QUADs, just in case damping is already engaged
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
        sleep(3.0)

        for sus in quads:
            for mode in modes2:
                if not ('ITMY' in sus and '13' in mode):
                # Clear damping filter histories for all QUADs
                    ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RSET'] = 2
                # Clear monitor filter histories for all QUADs
                    #ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_BL_RSET'] = 2
                    #ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RMSLP_RSET'] = 2
                
        # Set ITMX damping filters
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM2', 'FM8', 'FM10', 'ON')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM6', 'FM10', 'ON') # Removed FM1 DL 10/11/2023

        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM7', 'FM10', 'ON') # Removed FM1 DL 10/31/2024

        # Set ITMY damping filters
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM7', 'FM10', 'ON')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM7', 'FM9', 'FM10', 'ON')
        
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM8', 'FM10', 'ON')


        # Set ETMX damping filters
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM9', 'FM10', 'ON')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM3', 'FM10', 'ON') # removed FM2

        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE13',  'FM3', 'FM10', 'ON') # Changed from FM4 to FM3, DL 06-07-2024

        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM1', 'FM4', 'FM10', 'ON')

        
        # Set ETMY damping filters
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM2', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM2', 'FM4', 'FM10', 'ON')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM1', 'FM7', 'FM10', 'ON')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM1', 'FM4', 'FM10', 'ON')


        # Engage damping filter outputs for all QUADs
        for sus in quads:
            for mode in modes2:
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'ON')

        # Set ITMX damping filter gains
        ezca['SUS-ITMX_L2_DAMP_MODE11_GAIN'] = 100
        ezca['SUS-ITMX_L2_DAMP_MODE12_GAIN'] = -40
        ezca['SUS-ITMX_L2_DAMP_MODE13_GAIN'] = -200
        ezca['SUS-ITMX_L2_DAMP_MODE14_GAIN'] = -200 # Changed gain from 50 to -200 DL 10/11/2023

        # Set ITMY damping filter gains
        ezca['SUS-ITMY_L2_DAMP_MODE11_GAIN'] = -10
        ezca['SUS-ITMY_L2_DAMP_MODE12_GAIN'] = -10
        ezca['SUS-ITMY_L2_DAMP_MODE13_GAIN'] = -100
        ezca['SUS-ITMY_L2_DAMP_MODE14_GAIN'] = 10

        # Set ETMX damping filter gains
        ezca['SUS-ETMX_L2_DAMP_MODE11_GAIN'] = -40 # Changed gain from 40 to -40 after O4 vent, DL 2/21/2024
        ezca['SUS-ETMX_L2_DAMP_MODE12_GAIN'] = -10 # Changed gain from 40 to -40 after O4 vent, DL 2/21/2024
        ezca['SUS-ETMX_L2_DAMP_MODE13_GAIN'] = -50 # Changed gain from 40 to -40 after O4 vent, DL 2/21/2024
        ezca['SUS-ETMX_L2_DAMP_MODE14_GAIN'] = 40 # Changed gain from -40 to 40 after O4 vent, DL 2/21/2024

        # Set ETMY damping filter gains
        ezca['SUS-ETMY_L2_DAMP_MODE11_GAIN'] = 10
        ezca['SUS-ETMY_L2_DAMP_MODE12_GAIN'] = -10 # changed -40 to -10 DL 3/22/2024
        ezca['SUS-ETMY_L2_DAMP_MODE13_GAIN'] = 40
        ezca['SUS-ETMY_L2_DAMP_MODE14_GAIN'] = -80
        
        notify("2nd order violin mode damping engaged")
        sleep(3.0)

    def run(self):
        for sus in quads:
            for mode in modes2:
                level = ezca['SUS-'+sus+'_L2_DAMP_MODE'+mode+'_RMSLP_LOG10_OUTMON']
                index = ezca['GRD-ISC_LOCK_STATE_N']
                if (level >= -16.5) & (index >= 1100):
                    #notify("Warning! "+sus+" violin mode "+mode+" above threshold, gain set to zero")
                    notify("Warning! "+sus+" violin mode "+mode+" elevated")
                    ezca['SUS-'+sus+'_L2_DAMP_MODE'+mode+'_GAIN'] = 0 #reenabled functionality VB2-240417
        return True

class UNDAMP_2ND(GuardState):
    goto = True
    request = True

    def main(self):
        for sus in quads:
            for mode in modes2:
                # if not ('ITMY' in sus and '13' in mode):
                # Zero the gains for all QUADs
                    ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
                # Disengage damping filter outputs for all QUADs
                    ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'OFF')
        notify("2nd order violin mode damping disengaged")
        sleep(3.0)

    def run(self):
        return True

class UNDAMP_ALL(GuardState):
    goto = True
    request = True

    def main(self):
        for sus in quads:
            for mode in modes1 + modes2 + modes3:
                if not ('ITMY' in sus and '13' in mode):
                # Zero the gains for all QUADs
                    ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
                # Disengage damping filter outputs for all QUADs
                    ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'OFF')
        notify("All violin mode damping disengaged")
        sleep(3.0)

    def run(self):
        return True

edges = [
    ('INIT','IDLE'),
    ('IDLE','DAMP_2ND'),
    ('IDLE','UNDAMP_2ND'),
    ('IDLE','UNDAMP_ALL'),
    ]

# edges += [('DAMP_2ND','DAMP_2ND')]
edges += [('UNDAMP_2ND','UNDAMP_2ND')]
edges += [('UNDAMP_ALL','UNDAMP_ALL')]
